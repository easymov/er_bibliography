# er_bibliography

This jar contains extensions to handle the bibliography entries.

First, you need to create a file called *bib.md*.
Inside this file you can call the following extensions to add entry to your bibliography.

```
@bibliography.book('ref_book', title='Book', author='Norbert', publisher='Leon', year='2018')
@bibliography.manual('ref_manual', title='Manual', author='Albert', organization='Dumas', year='2018')
@bibliography.misc('ref_website', title='Misc', author='Open Robotics', note='http://www.ros.org/is-ros-for-me/', year='2018')
@bibliography.phdthesis('ref_thesis', title='Php Thesis', author='Lambert', school='ISIMA', year='2018')
```

**Do not put anything else than the calls to those extensions inside the *bib.md* file**.
**If you do the project will probably not work.**

If you do not cite an entry it will not appear in the bibliography.
To cite something you need to use the following extension:

```
ROS has a permissive Licensing system @cite('ref_website').
```